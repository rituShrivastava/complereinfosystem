function findpairs(arr, targetedNum) {
  if (arr.length < 2) {
    return [];
  }
  const numArr = {};
  for (let i = 0; i < arr.length; i++) {
    let num1 = numArr[arr[i]];
    console.log(num1);
    if (num1 >= 0) {
      return [i, num1];
    } else {
      const numberToFind = targetedNum - arr[i];
      numArr[numberToFind] = i;
      console.log(i);
    }
  }
  return [];
}

let nums = [2, 7, 11, 15];
let target = 9;
findpairs(nums, target);